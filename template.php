<?php

/**
  * @file
  * Here we override the default HTML output of drupal.
  * refer to https://drupal.org/node/457740
  *
  * Note that many of the functions in this file originated in the
  * Basic starter theme. Basic's maintainers encourage the themer to use
  * their package as starter files for the custom theme, rather than
  * subtheming it.
  */


/**
  *  Implements theme_menu_tree, adding menu name as ul class. This is done via
  *  theme function rather than theme preprocess function because the latter
  *  lacks access to the menu trees html attributes array.
  *
  *  NOTE: devel_themer module makes theme_hook_original variable inaccessible.
  */
function themename_menu_tree($variables) {
  $menu_name = drupal_clean_css_identifier(
    str_replace('menu_tree__', '', $variables['theme_hook_original'])
  );
  $output = '<ul class="menu ' . $menu_name . '">' . $variables['tree'] . '</ul>';
  return $output;
}

/**
 * Implements theme_preprocess_menu_link.
 * Adds class to indicate menu depth of item.
 */
function acmp_neat_preprocess_menu_link(&$variables) {
  $depth = $variables['element']['#original_link']['depth'];
  $variables['element']['#attributes']['class'][] = 'depth-' . $depth;
}

/**
 *  All functions below this line originated from the Basic starter theme.
 *  =======================================================================
 */


/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $variables
 *  An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *  A themed HTML string.
 *
 * @ingroup themeable
 *
 */

function themename_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = themename_id_safe($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  if (isset($element['#original_link']['mlid']) && !empty($element['#original_link']['mlid'])) {
    $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  }
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('clear_registry')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

// Add Zen Tabs styles
if (theme_get_setting('themename_tabs')) {
  drupal_add_css( drupal_get_path('theme', 'themename') .'/css/tabs.css');
}

function themename_preprocess_html(&$variables) {
  global $user, $language;

  // Add role name classes (to allow css based show for admin/hidden from user)
  foreach ($user->roles as $role){
    $variables['classes_array'][] = 'role-' . themename_id_safe($role);
  }

  // HTML Attributes
  // Use a proper attributes array for the html attributes.
  $variables['html_attributes'] = array();
  $variables['html_attributes']['lang'][] = $language->language;
  $variables['html_attributes']['dir'][] = $language->dir;

  // Convert RDF Namespaces into structured data using drupal_attributes.
  $variables['rdf_namespaces'] = array();
  if (function_exists('rdf_get_namespaces')) {
    foreach (rdf_get_namespaces() as $prefix => $uri) {
      $prefixes[] = $prefix . ': ' . $uri;
    }
    $variables['rdf_namespaces']['prefix'] = implode(' ', $prefixes);
  }

  // Flatten the HTML attributes and RDF namespaces arrays.
  $variables['html_attributes'] = drupal_attributes($variables['html_attributes']);
  $variables['rdf_namespaces'] = drupal_attributes($variables['rdf_namespaces']);

  if (!$variables['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $variables['classes_array'][] = 'with-subnav';
    $variables['classes_array'][] = themename_id_safe('page-'. $path);
    $variables['classes_array'][] = themename_id_safe('section-'. $section);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          // Remove 'section-node'
          array_pop( $variables['classes_array'] );
        }
        // Add 'section-node-add'
        $variables['classes_array'][] = 'section-node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          // Remove 'section-node'
          array_pop( $variables['classes_array']);
        }
        // Add 'section-node-edit' or 'section-node-delete'
        $variables['classes_array'][] = 'section-node-'. arg(2);
      }
    }
  }
  //for normal un-themed edit pages
  if ((arg(0) == 'node') && (arg(2) == 'edit')) {
    $variables['template_files'][] =  'page';
  }

  // // Add IE classes.
  // if (theme_get_setting('themename_ie_enabled')) {
  //   $themename_ie_enabled_versions = theme_get_setting('themename_ie_enabled_versions');
  //   if (in_array('ie8', $themename_ie_enabled_versions, TRUE)) {
  //     drupal_add_css(path_to_theme() . '/css/ie8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
  //     drupal_add_js(path_to_theme() . '/js/build/selectivizr-min.js');
  //   }
  //   if (in_array('ie9', $themename_ie_enabled_versions, TRUE)) {
  //     drupal_add_css(path_to_theme() . '/css/ie9.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 9', '!IE' => FALSE), 'preprocess' => FALSE));
  //   }
  //   if (in_array('ie10', $themename_ie_enabled_versions, TRUE)) {
  //     drupal_add_css(path_to_theme() . '/css/ie10.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 10', '!IE' => FALSE), 'preprocess' => FALSE));
  //   }
  // }

}

function themename_preprocess_page(&$variables, $hook) {

  if (isset($variables['node_title'])) {
    $variables['title'] = $variables['node_title'];
  }
  // Adding classes whether #navigation is here or not
  if (!empty($variables['main_menu']) or !empty($variables['sub_menu'])) {
    $variables['classes_array'][] = 'with-navigation';
  }
  if (!empty($variables['secondary_menu'])) {
    $variables['classes_array'][] = 'with-subnav';
  }

  // Allow page override template suggestions based on node content type.
  if (isset($variables['node']->type) && isset($variables['node']->nid)) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
    $variables['theme_hook_suggestions'][] = "page__node__" . $variables['node']->nid;
  }
}

function themename_preprocess_node(&$variables) {
  // Add $unpublished variable.
  $variables['unpublished'] = (!$variables['status']) ? TRUE : FALSE;
}

/**
 * Implements theme_preprocess_block.
 */
function themename_preprocess_block(&$variables, $hook) {
  /*
    Only use classes defined via block name or via the block_class module.
    Modified by Jeff Hipp from the original Basic theme function,
    which only retained the .block class.
    NOTE: block_class value is commented out, as it causes errors if
    block_class module is not implemented
  */
  $variables['classes_array'] = array($variables['block_html_id']/*, $variables['block']->css_class*/);
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function themename_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('themename_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('themename_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('themename_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('themename_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $title = check_plain($item['title']);
        }
        else {
          $title = drupal_get_title();
        }
        if ($title) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('themename_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users. Make the heading invisible with .element-invisible.
      $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

      return $heading . '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</div>';
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *  The string
 * @return
 *  The converted string
 */
function themename_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Override or insert variables into theme_menu_local_task().
 */
function themename_preprocess_menu_local_task(&$variables) {
  $link =& $variables['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab ' . drupal_html_class('task-' . $link['title']) . '">' . $link['title'] . '</span>';
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function themename_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}
