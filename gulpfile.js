var gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('gulp-autoprefixer');
  minify = require('gulp-minify');
  browserSync = require('browser-sync').create();

gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 3 versions'],
    cascade: false
  }))
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('./css'))
});

gulp.task('js-minify', function() {
  gulp.src('js/source/*.js')
    .pipe(minify({
      ext:{
          src:'.js',
          min:'-min.js'
      },
      noSource: ['*']
    }))
    .pipe(gulp.dest('js/build'))
});

gulp.task ('sass-watch', function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
});

// gulp.task ('css-watch', function () {
//   gulp.watch("./style.css").on('change', browserSync.reload);
// });

gulp.task ('js-watch', function () {
  gulp.watch('./js/source/*.js', ['js-minify']);
})

gulp.task('default', ['sass','sass-watch', 'js-watch']);
