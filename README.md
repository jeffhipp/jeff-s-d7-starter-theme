# Jeff Hipp's D7 Theme Template
v1.2 | June 2016

A Drupal 7 theme template based on the [Basic starter theme](https://www.drupal.org/project/basic). It utilizes HTML5 templates and [SMACSS](https://smacss.com/)-based Sass file architecture, along with the [Neat Grid Framework](http://neat.bourbon.io/) and [CSS Reset.](http://meyerweb.com/eric/tools/css/reset/)

The included Gulp file minifies JS on save and compiles, maps, and auto-prefixes Sass on save.

## Get Started
* Run `npm-install` to download gulp and its dependencies.
* Run `gulp` to initialize gulp and have it begin watching JS and SCSS files.
* Review scss/style.scss to understand the different partial files it utilizes.
* scss/base/_globalvars.scss is a good place to begin customizing the project CSS.

## Notes
* Much of the template.php and tpl.php files are from the Basic starter theme. Basic encourages themers to do just this, rather than sub-theme it -- take it and modify its contents to make it your own.
* scss partial files that only contain variables or mixins utilize a set of single line comments in the doc block rather than block commenting. This is done to prevent them from appearing in the css output.
* gulp-sass is set to produce compressed css output. You may change this setting in the Sass gulp task on the following line, as per [node-sass documentation](https://github.com/sass/node-sass#user-content-outputstyle): 

```
#!Javascript

.pipe(sass({outputStyle: 'outputNameHere'}).on('error', sass.logError))
```

## Todo
* Get Browsersync CSS refreshes working again (They broke when I added Sass Maps).